#ifndef _DEFS_H_
#define _DEFS_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <fcntl.h>
#include <signal.h>
#include <math.h>


#include <pthread.h>

#ifndef NULL
#define NULL 0L
#endif /* ~NULL */

#endif /*_DEFS_H_*/
