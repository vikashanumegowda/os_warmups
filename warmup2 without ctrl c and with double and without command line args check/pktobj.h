#ifndef _PKT_OBJ_H_
#define _PKT_OBJ_H_


typedef struct PktObj {
	double inter_arrival_time;// this is 1/lambda
	double arrival_time;
	int num_tokens;//number of tokens required
	int pkt_num;//packet ID or number
	double q1_arrival_time;
	double q2_arrival_time;
	double s_entry_time;	
	double s1_entry_time;
	double s1_exit_time;
	double s2_entry_time;
	double s2_exit_time;
	double s_exit_time;
	double mu;
	double service_time;//expected service time(from t_file) 1/mu
} Obj;



#endif /*_PKT_OBJ_H_*/
