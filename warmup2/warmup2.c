#include "defs.h"
#include "pktobj.h"
#include "my402list.h"

struct stat directory;
My402List *q1,*q2,*q3;
double lambda=1,mu=0.35,r=1.5;//lambda=> packet rate;mu=> server rate;r=> token rate;
long B=10,P=3,n=20,m_for_n=20,b_tokens=0,tot_tokens=0;//bucket size; num of tokens reqd. per packet, n=> total packets to be processed; b_tokens=> number of tokens in the bucket at a particular moment of time; tot_tokens=> total number of tokens generated
int num_packets=0,num_s_packets;//number of packets generated; number of packets processed at either of the servers
int num_pkts_1to2=0,num_pkts_2to3=0,terminate_s=0,s1_terminated=0,s2_terminated=0,packets_done=0,token_done=0,tok_drop_count=0,pkt_drop_count=0,token_cancelled=0,normal_execution_ended=0;
char tsfile[200];
double start_time=0.0,prev_p_arrival,tok_current_time=0.0,pkt_current_time=0.0,s1_current_time=0.0,s2_current_time=0.0;
int servers_exit_for_int_handler=0;
//int n_excluding_drop_pkts=n;
int q_not_empty=0;
static char gszProgName[MAXPATHLENGTH];
int gnDebug=0;
FILE *fp;

int token_thread_cancel=0;
int packet_thread_cancel=0;
int server_thread_cancel=0;
int thread_ended_ctrl_c=0;

pthread_t token_t, packet_t, s1_t, s2_t,cc_thread;
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cv = PTHREAD_COND_INITIALIZER;
sigset_t set;




void get_time(double *ret)
{
	struct timeval tv;
	gettimeofday(&tv,NULL);
	*ret=(double)(tv.tv_sec*1000000+tv.tv_usec);
	
}

void print_time(double t)
{
	double time_t = t-start_time;
	printf("%012.3lfms: ",time_t/1000.0);
}

static
void Usage()
{
    fprintf(stderr,
            "./warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n"
            /*,gszProgName, "test"*/);
    exit(-1);
}

static
void ProcessOptions(int argc, char *argv[])
{
	int read_number=0;
	if(argc%2==0)
	{
		printf("(Malformed command line)\n");
		Usage();
	}
    for (argc--, argv++; argc > 0; argc--, argv++) {
	if (*argv[0] == '-') {
            if (strcmp(*argv, "-lambda") == 0) {
                read_number=sscanf(*(++argv), "%lf", &lambda);
		if(read_number!=1)
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		argc--;
		if(isnan(lambda))
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		if(lambda>=0)
			continue;
		else
		{
			printf("lambda value is negative\n");
			exit(1);
		}
            }
		else if (strcmp(*argv, "-mu") == 0) {
                read_number=sscanf(*(++argv), "%lf", &mu);
		if(read_number!=1)
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		argc--;
		if(isnan(mu))
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		if(mu>=0)
			continue;
		else
		{
			printf("mu value is negative\n");
			exit(1);
		}
            }
		else if (strcmp(*argv, "-r") == 0) {
                read_number=sscanf(*(++argv), "%lf", &r);
		if(read_number!=1)
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		argc--;
		if(isnan(r))
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		if(r>=0)
			continue;
		else
		{
			printf("r value is negative\n");
			exit(1);
		}
            }
		else if (strcmp(*argv, "-B") == 0) {
                read_number=sscanf(*(++argv), "%ld", &B);
		if(read_number!=1)
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		argc--;
		if(isnan(B))
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		if(B>0 && B<=2147483647)
			continue;
		else
		{
			printf("B value is invalid\n");
			exit(1);
		}
            }		
		else if (strcmp(*argv, "-P") == 0) {
                read_number=sscanf(*(++argv), "%ld", &P);
		if(read_number!=1)
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		argc--;
		if(isnan(P))
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		if(P>0 && P<=2147483647)
			continue;
		else
		{
			printf("P value is invalid\n");
			exit(1);
		}
            }	
		else if (strcmp(*argv, "-n") == 0) {
                read_number=sscanf(*(++argv), "%ld", &n);
		m_for_n=n;
		if(read_number!=1)
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		argc--;
		if(n>0 && n<=2147483647)
			continue;
		else
		{
			printf("n value is invalid\n");
			exit(1);
		}
            }	
		else if (strcmp(*argv, "-t") == 0) {
                read_number=sscanf(*(++argv), "%s", tsfile);
		if(read_number!=1)
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}
		argc--;
            }
		else
		{
			printf("(malformed command)\n");
			Usage();
			exit(1);
		}	
        }
	else 
	{
		printf("(malformed command)\n");
			Usage();
			exit(1);
	}
	read_number=0;
    }
}

static
void SetProgramName(char *s)
{
    char *c_ptr=strrchr(s, DIR_SEP);

    if (c_ptr == NULL) {
        strcpy(gszProgName, s);
    } else {
        strcpy(gszProgName, ++c_ptr);
    }
}


void Traverse(My402List *list)//for debug
    {
        My402ListElem *elem=NULL;

        for (elem=My402ListFirst(list);
                elem != NULL;
                elem=My402ListNext(list, elem)) {
            Obj *foo=(Obj*)(elem->obj);
		printf("%d\n",foo->pkt_num);
            /* access foo here */
        }
    }


void cleanup_push_handler_pkt(void* arg)
{
	pthread_mutex_lock(&m);
	if(fp)	
		fclose(fp);
	pthread_mutex_unlock(&m);
}


void *packet_handler(void *arg)
{	
	pthread_cleanup_push(cleanup_push_handler_pkt, (void *) 0);
	double inter_arrival_time=0.0;
	double service_time=0.0,prev_pkt_arrival_time=start_time;
	double adjustment_time=0.0;
	char buff[40];
	long P1;
	Obj *firstobj;//, *lastobj;//
	//My402ListElem* q2last;//
	My402ListElem* first;//
	/*
	3
	inter-arrival time(1/l)	P	service time(1/mu)	
    	2716   			2    	9253
    	7721   			1   	15149
    	972    			3    	2614
	*/
	if(strcmp(tsfile,"")!=0)//if file name present in the command
	{	
			if(r<0.1)
				r=0.1;
			while(fgets(buff,sizeof(buff),fp)!=NULL)
			{
				sscanf(buff,"%lf%ld%lf\n",&inter_arrival_time,&P1,&service_time);
				//printf("Read %lf %ld %lf\n",inter_arrival_time,P1,service_time);
				/*inter_arrival_time=1.0/lambda;		
				if(inter_arrival_time>=10)
					inter_arrival_time=10.0;

				service_time=1.0/mu;
				if(service_time>=10)
					service_time=10.0;*/
				
				get_time(&pkt_current_time);
				adjustment_time=(inter_arrival_time*1000.0)-(pkt_current_time-prev_pkt_arrival_time);
				//get_time(&prev_pkt_arrival_time);
				if(adjustment_time>=0)
					usleep(adjustment_time);
				else
					usleep(0.0);
				
				num_packets++;	
				
				Obj* newObj=(Obj*)malloc(sizeof(Obj));
				get_time(&newObj->arrival_time);
				//prev_pkt_arrival_time=newObj->arrival_time;
				newObj->num_tokens=P1;
				newObj->pkt_num=num_packets;
				//printf("%d\n",newObj->pkt_num);
				newObj->q1_arrival_time=0.0;		
				newObj->q2_arrival_time=0.0;
				newObj->mu=1000.0/service_time;
				newObj->service_time=service_time*1000.0;
				

				get_time(&pkt_current_time);	
				//print_time(pkt_current_time);
				newObj->inter_arrival_time=(pkt_current_time-prev_pkt_arrival_time)/1000.0;	
				prev_pkt_arrival_time=pkt_current_time;		
				if(newObj->num_tokens>B)	
				{
					pkt_drop_count++;
					m_for_n--;
					//n_excluding_drop_pkts--;
					get_time(&pkt_current_time);	
					print_time(pkt_current_time);
					printf("p%d arrives, needs %d tokens, inter_arrival time = %.3lfms, dropped\n",newObj->pkt_num,newObj->num_tokens,newObj->inter_arrival_time);
					free(newObj);	
					pthread_cond_broadcast(&cv);		
					continue;		
				}
		
				get_time(&pkt_current_time);	
				print_time(pkt_current_time);
				printf("p%d arrives, needs %d tokens, inter_arrival time = %.3lfms\n",newObj->pkt_num,newObj->num_tokens,newObj->inter_arrival_time);

				
				pthread_mutex_lock(&m);
	
				get_time(&newObj->q1_arrival_time);
				print_time(newObj->q1_arrival_time);
				printf("p%d enters Q1\n",newObj->pkt_num);
		
				if(!My402ListEmpty(q1))
					My402ListAppend(q1,newObj);
				else
				{
					My402ListAppend(q1,newObj);
					if(b_tokens>=newObj->num_tokens)
					{
						b_tokens-=newObj->num_tokens;
						get_time(&pkt_current_time);
						print_time(pkt_current_time);
						get_time(&newObj->q2_arrival_time);
						printf("p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %d tokens\n",newObj->pkt_num,(newObj->q2_arrival_time-newObj->q1_arrival_time),(int)b_tokens);
						print_time(newObj->q2_arrival_time);				
						printf("p%d enters Q2\n",newObj->pkt_num);
						first= (My402ListElem*)My402ListFirst(q1);
						firstobj=(Obj*)first->obj;
						My402ListAppend(q2, firstobj);
						My402ListUnlink(q1, My402ListFirst(q1));
						num_pkts_1to2++;
					}
					else
					{
						pthread_mutex_unlock(&m);
						continue;
					}
					pthread_cond_broadcast(&cv);	
				}
				pthread_mutex_unlock(&m);
		
			}
			
		packets_done=1;
	}
	else{
		while(num_packets<n)
		{
			inter_arrival_time=1.0/lambda;		
			if(inter_arrival_time>=10)
				inter_arrival_time=10.0;

			service_time=1.0/mu;
			if(service_time>=10){
				service_time=10.0;
				mu=0.1;
			}
			if(r<0.1)
				r=0.1;
			get_time(&pkt_current_time);
			adjustment_time=(inter_arrival_time*1000000)-(pkt_current_time-prev_pkt_arrival_time);
			if(adjustment_time>=0)
				usleep(adjustment_time);
			else
				usleep(0.0);

			
			num_packets++;
			Obj* newObj=(Obj*)malloc(sizeof(Obj));
			
			
			
			get_time(&newObj->arrival_time);
			newObj->num_tokens=P;
			newObj->pkt_num=num_packets;
			newObj->q1_arrival_time=0.0;		
			newObj->q2_arrival_time=0.0;
			newObj->mu=mu;
			newObj->service_time=service_time*1000.0;
		
		
			get_time(&pkt_current_time);	
			//print_time(pkt_current_time);
			newObj->inter_arrival_time=(pkt_current_time-prev_pkt_arrival_time)/1000.0;
			get_time(&pkt_current_time);
			prev_pkt_arrival_time=pkt_current_time;		
			if(newObj->num_tokens>B)	
			{
				pkt_drop_count++;
				m_for_n--;
				//n_excluding_drop_pkts--;
				get_time(&pkt_current_time);	
				print_time(pkt_current_time);
				printf("p%d arrives, needs %d tokens, inter_arrival time = %.3lfms, dropped\n",num_packets,newObj->num_tokens,newObj->inter_arrival_time);
				free(newObj);
				pthread_cond_broadcast(&cv);			
				continue;		
			}
				
			get_time(&pkt_current_time);	
			print_time(pkt_current_time);
			printf("p%d arrives, needs %d tokens, inter_arrival time = %.3lfms\n",num_packets,newObj->num_tokens,newObj->inter_arrival_time);

			
			pthread_mutex_lock(&m);
	
			get_time(&newObj->q1_arrival_time);
			print_time(newObj->q1_arrival_time);
			printf("p%d enters Q1\n",num_packets);
		
			if(!My402ListEmpty(q1))
				My402ListAppend(q1,newObj);
			else
			{
				My402ListAppend(q1,newObj);
				if(b_tokens>=newObj->num_tokens)
				{
					b_tokens-=newObj->num_tokens;
					get_time(&pkt_current_time);
					print_time(pkt_current_time);
					get_time(&newObj->q2_arrival_time);
					printf("p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %d tokens\n",num_packets,(newObj->q2_arrival_time-newObj->q1_arrival_time),(int)b_tokens);
					print_time(newObj->q2_arrival_time);				
					printf("p%d enters Q2\n",num_packets);
					
					first= (My402ListElem*)My402ListFirst(q1);
					firstobj=(Obj*)first->obj;
					//printf("%d\n",(int)(firstobj->num_tokens));//
					My402ListAppend(q2,firstobj);
					My402ListUnlink(q1, My402ListFirst(q1));
					//q2last=(My402ListElem*)My402ListLast(q2);//
					//lastobj=(Obj*)q2last->obj;		//
					//printf("%d\n",(int)(lastobj->num_tokens));// */
					num_pkts_1to2++;
				}
				else
					{
						pthread_mutex_unlock(&m);
						continue;
					}
				pthread_cond_broadcast(&cv);	
			}
			pthread_mutex_unlock(&m);
		
		}
	}
	//no_more_packets=1;
	//printf("Packet arrival end\n");
	pthread_cleanup_pop(1);
	return 0;
}


void cleanup_push_handler_tok(void* arg)
{
	pthread_mutex_lock(&m);
	
		token_cancelled=1;
	pthread_cond_broadcast(&cv);
	pthread_mutex_unlock(&m);
}

void *token_handler(void *arg)
{
	pthread_cleanup_push(cleanup_push_handler_tok, (void *) 0);
	Obj *firstobj;//, *lastobj;
	//My402ListElem* q2last;
	My402ListElem* first;
	double adjustment_time=0.0;
	double prev_tok_arrival_time=0.0;
	//=(My402ListElem*)malloc(sizeof(My402ListElem));
	//printf("Token thread start\n");
	
	prev_tok_arrival_time=start_time;
	while(num_pkts_1to2<m_for_n)// && !no_more_packets)	
	{
		get_time(&tok_current_time);
		adjustment_time=(1000000.0/r)-(tok_current_time-prev_tok_arrival_time);
		
		if(adjustment_time>=0)
			usleep(adjustment_time);
		else
			usleep(0.0);
		
		pthread_mutex_lock(&m);
		if(b_tokens<B)
		{
			b_tokens++;
			tot_tokens++;
			
			get_time(&tok_current_time);
			print_time(tok_current_time);
			printf("token t%d arrives, token bucket now has %d tokens\n",(int)tot_tokens,(int)b_tokens);
			
			prev_tok_arrival_time=tok_current_time;
			
			if(!My402ListEmpty(q1))
			{
				first=(My402ListElem*)My402ListFirst(q1);
				firstobj=(Obj*)first->obj;
				
				if(firstobj->num_tokens<=b_tokens)
				{
					b_tokens -= firstobj->num_tokens;
					
					get_time(&tok_current_time);
					firstobj->q2_arrival_time=tok_current_time;
					print_time(tok_current_time);
					printf("p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %d tokens\n",firstobj->pkt_num,(firstobj->q2_arrival_time-firstobj->q1_arrival_time)/1000.0,(int)b_tokens);
					
					get_time(&tok_current_time);
					print_time(tok_current_time);
					printf("p%d enters Q2\n",firstobj->pkt_num);
					
					first= (My402ListElem*)My402ListFirst(q1);
					firstobj=(Obj*)first->obj;
					//printf("%d\n",(int)(firstobj->num_tokens));//
					My402ListAppend(q2,firstobj);
					My402ListUnlink(q1, My402ListFirst(q1));
					//q2last=(My402ListElem*)My402ListLast(q2);//
					//lastobj=(Obj*)q2last->obj;		//
					//printf("%d\n",(int)(lastobj->num_tokens));//
					num_pkts_1to2++;
					pthread_cond_broadcast(&cv);	
					/*
					first=(My402ListElem*)My402ListFirst(q2);
					firstobj=(Obj*)first->obj;
					printf("%d\n",(int)(firstobj->num_tokens));
					My402ListAppend(q3, firstobj);		
					My402ListUnlink(q2, first);
					q3last=(My402ListElem*)My402ListLast(q3);
					lastobj=(Obj*)q3last->obj;		
					printf("%d\n",(int)(lastobj->num_tokens));
					*/
				}
				else if(firstobj->num_tokens>B)
				{
					pkt_drop_count++;
					m_for_n--;
					//n_excluding_drop_pkts--;
					get_time(&pkt_current_time);	
					print_time(pkt_current_time);
					printf("p%d arrives, needs %d tokens, inter_arrival time = %.3lfms, dropped\n",firstobj->pkt_num,firstobj->num_tokens,firstobj->inter_arrival_time);
					free(firstobj);	
					pthread_cond_broadcast(&cv);		
					continue;
				}
			}		
			
		}
		else
		{
			
			tot_tokens++;
			tok_drop_count++;
			
			get_time(&tok_current_time);
			print_time(tok_current_time);
			printf("token t%d arrives, dropped\n",(int)tot_tokens);
			
			prev_tok_arrival_time=tok_current_time;
		}
		pthread_mutex_unlock(&m);
		//Traverse(q2);
		//printf("%d\n 1to2",num_pkts_1to2);		
	}
	token_done=1;
	//no_more_tokens=1;
	
	//printf("Token thread finished1\n");
	pthread_cleanup_pop(1);
	
	//printf("Token thread finished2\n");
	
	return 0;
}

void *server_handler1(void *arg)
{
	My402ListElem *first,*q3last;
	Obj *lastobj,*firstobj;
	
	double adjustment_time=0.0,prev_s1_time=0.0;
	double pkt_process_start_time=0.0,end_time=0.0;
	//printf("Server1\n");
	prev_s1_time=start_time;
	while(TRUE) {
		pthread_mutex_lock(&m);
		//printf("hello\n");
		if(num_pkts_2to3>=m_for_n)// || no_more_tokens)
		{	
			//printf("%d, %ld\n",num_pkts_2to3,m_for_n);
			//printf("before cond wait1 in 1\n");
			pthread_mutex_unlock(&m);
			break;
		}
		while (My402ListEmpty(q2) && num_pkts_2to3<m_for_n)//&&!no_more_tokens){
		{	
			if(token_cancelled )
				break;
			//printf("before cond wait2 in 1\n");
		    	pthread_cond_wait(&cv, &m);
			
		}
		if(num_pkts_2to3>=m_for_n ||token_cancelled)//||no_more_tokens)
		{
			pthread_mutex_unlock(&m);
			break;
		}
		//printf("hello\n");
		//printf("hello\n");
		first=(My402ListElem*)My402ListFirst(q2);
		firstobj=(Obj*)first->obj;
		//printf("%d\n",(int)(firstobj->num_tokens));
		My402ListAppend(q3, firstobj);		
		My402ListUnlink(q2, first);
		num_pkts_2to3++;
		q3last=(My402ListElem*)My402ListLast(q3);
		lastobj=(Obj*)q3last->obj;		
		//printf("%d\n",(int)(lastobj->num_tokens));
		
		get_time(&s1_current_time);
		pkt_process_start_time=s1_current_time;
		lastobj->s1_entry_time=s1_current_time;
		lastobj->s_entry_time=s1_current_time;
		lastobj->s2_entry_time=0.0;
		prev_s1_time=s1_current_time;
		print_time(s1_current_time);
		printf("p%d begins service at s1, requesting %.0lfms of service\n",lastobj->pkt_num,(1000.0/lastobj->mu));		
		get_time(&s1_current_time);
		adjustment_time=(1000000.0/lastobj->mu)-(s1_current_time-prev_s1_time);
		
		pthread_mutex_unlock(&m);
	
		if(adjustment_time>=0)
			usleep(adjustment_time);
		else
			usleep(0.0);
		
		
		// dequeue a job and/or other stuff 
		pthread_mutex_lock(&m);
		
		get_time(&s1_current_time);
		end_time=s1_current_time;
		lastobj->s1_exit_time=end_time;
		lastobj->s_exit_time=end_time;
		lastobj->s2_exit_time=0.0;
		lastobj->service_time=end_time-pkt_process_start_time;
		print_time(s1_current_time);
		printf("p%d departs from s1, service time =  %.3lfms, time in system = %.3lfms\n",lastobj->pkt_num,lastobj->service_time/1000.0,(s1_current_time-lastobj->arrival_time)/1000.0);
			
		pthread_mutex_unlock(&m);
		if(token_cancelled )
		{
			pthread_mutex_unlock(&m);
			break;
		}
		//Traverse(q3);
		//printf("asd\n");
		//Traverse(q2);
		// work on the job based on its service_time 
    	}
	//printf("End of Servicing at s1\n");
	
	servers_exit_for_int_handler++;
	return 0;
}
	
void *server_handler2(void *arg)
{
	//printf("Server2\n");
	My402ListElem *first,*q3last;
	Obj *lastobj,*firstobj;
	
	double adjustment_time=0.0,prev_s2_time=0.0;
	double pkt_process_start_time=0.0,end_time=0.0;

	prev_s2_time=start_time;
	while(TRUE) {
		
		pthread_mutex_lock(&m);
		//printf("hello\n");
		if(num_pkts_2to3>=m_for_n)// || no_more_tokens)
		{	
			//printf("%d, %ld\n",num_pkts_2to3,m_for_n);
			//printf("before cond wait1 in 2\n");
			pthread_mutex_unlock(&m);
			break;
		}
		while (My402ListEmpty(q2) && num_pkts_2to3<m_for_n)// && !no_more_tokens))
		{
			//printf("%d, %d, %d, %d\n",num_pkts_2to3,num_pkts_1to2,token_done,!(num_pkts_2to3!=0&&num_pkts_2to3==m_for_n&&token_done));
			//printf("before cond wait2 in 2\n");			
			if(n==1)//only for one of the servers for n==1 - DO NOT copy this to server_handler 1
			{
				pthread_mutex_unlock(&m);
				break;
			}
			if(token_cancelled)
				break;
		    	pthread_cond_wait(&cv, &m);
		}

		if(n==1)// || no_more_tokens)
		{
			pthread_mutex_unlock(&m);
			break;
		}
		if(num_pkts_2to3>=m_for_n || token_cancelled )//||no_more_tokens)
		{
			pthread_mutex_unlock(&m);
			break;
		}
		//printf("hello\n");
		first=(My402ListElem*)My402ListFirst(q2);
		firstobj=(Obj*)first->obj;
		////printf("%d\n",(int)(firstobj->num_tokens));
		My402ListAppend(q3, firstobj);		
		My402ListUnlink(q2, first);
		num_pkts_2to3++;
		q3last=(My402ListElem*)My402ListLast(q3);
		lastobj=(Obj*)q3last->obj;		
		//printf("%d\n",(int)(lastobj->num_tokens));
		
		get_time(&s2_current_time);
		pkt_process_start_time=s2_current_time;
		lastobj->s2_entry_time=s2_current_time;
		lastobj->s_entry_time=s2_current_time;
		lastobj->s1_entry_time=0.0;
		prev_s2_time=s2_current_time;
		print_time(s2_current_time);
		printf("p%d begins service at s2, requesting %.0lfms of service\n",lastobj->pkt_num,1000.0/lastobj->mu);
				
		get_time(&s2_current_time);
		adjustment_time=(1000000.0/lastobj->mu)-(s2_current_time-prev_s2_time);
		
		pthread_mutex_unlock(&m);
	
		if(adjustment_time>=0)
			usleep(adjustment_time);
		else
			usleep(0.0);
		
		
		// dequeue a job and/or other stuff 
		pthread_mutex_lock(&m);
		
		get_time(&s2_current_time);
		end_time=s2_current_time;
		lastobj->s2_exit_time=end_time;
		lastobj->s_exit_time=end_time;
		lastobj->s1_exit_time=0.0;
		lastobj->service_time=end_time-pkt_process_start_time;
		print_time(s2_current_time);
		printf("p%d departs from s2, service time =  %.3lfms, time in system = %.3lfms\n",lastobj->pkt_num,lastobj->service_time/1000.0,(s2_current_time-lastobj->arrival_time)/1000.0);
		

		
		pthread_mutex_unlock(&m);
				

		if(token_cancelled )
		{
			pthread_mutex_unlock(&m);
			break;
		}
		//Traverse(q3);
		//printf("asd\n");
		//Traverse(q2);
		// work on the job based on its service_time 
    	}
	
	servers_exit_for_int_handler++;
	//printf("End of Servicing at s2\n");
	return 0;
}

void print_statistics(double tot_emulation_time)
{	
	//Travserse edited
	My402ListElem *elem=NULL;
	int c=0;
	double sum_inter_arrival_time=0.0,sum_service_time=0.0,sum_time_in_q1=0.0;
	double sum_time_in_q2=0.0,sum_time_in_s1=0.0,sum_time_in_s2=0.0;
	double sum_sys_time=0.0,v,sum_of_sq_time=0.0;
        for (elem=My402ListFirst(q3);elem != NULL;elem=My402ListNext(q3, elem)) 
	{	
		Obj *foo=(Obj*)(elem->obj);
		c++;
		/*if(c==1 || c==2)
		{
			printf("foo->inter_arrival_time %.3lf\n",foo->inter_arrival_time);
			printf("foo->arrival_time %.3lf\n",foo->arrival_time);
			printf("foo->num_tokens %d\n",foo->num_tokens);
			printf("foo->pkt_num %d\n",foo->pkt_num);
			printf("foo->q1_arrival_time %.3lf\n",foo->q1_arrival_time);
			printf("foo->q2_arrival_time %.3lf\n",foo->q2_arrival_time);
			printf("foo->s_entry_time %.3lf\n",foo->s_entry_time);
			printf("foo->s1_entry_time %.3lf\n",foo->s1_entry_time);
			printf("foo->s1_exit_time %.3lf\n",foo->s1_exit_time);
			printf("foo->s2_entry_time %.3lf\n",foo->s2_entry_time);
			printf("foo->s2_exit_time %.3lf\n",foo->s2_exit_time);
			printf("foo->s_exit_time %.3lf\n",foo->s_exit_time);
			printf("foo->mu %.3lf\n",foo->mu);
			printf("foo->service_time %.3lf\n",foo->service_time);
		}*/
		
            	
		sum_inter_arrival_time += foo->inter_arrival_time;
		sum_service_time += foo->service_time;
		sum_time_in_q1 += (foo->q2_arrival_time - foo->q1_arrival_time);
		sum_time_in_q2 += (foo->s_entry_time - foo->q2_arrival_time);
		sum_time_in_s1 += (foo->s1_exit_time - foo->s1_entry_time);
		sum_time_in_s2 += (foo->s2_exit_time - foo->s2_entry_time);
		sum_sys_time += (foo->s_exit_time - foo->arrival_time);
		sum_of_sq_time += ((foo->s_exit_time - foo->arrival_time)/1000000.0*(foo->s_exit_time - foo->arrival_time)/1000000.0);
		//printf("%d\n",foo->pkt_num);
            /* access foo here */
        }



	//DO THIS







	printf("\nStatistics:\n\n");
	if(m_for_n!=0)
	{
		printf("\taverage packet inter-arrival time = %lfs\n",(double)(sum_inter_arrival_time/(m_for_n*1000.0)));
		printf("\taverage packet service time = %lfs\n\n",(double)((sum_service_time*1.0)/(m_for_n*1000000.0)));
	}
	else
	{
		printf("\taverage packet inter-arrival time = N/A because the no. of pkts reaching the servers is 0\nThus, avg. can't be calculated(Divide by zero error)'\n");
		printf("\taverage packet service time = N/A because the no. of pkts reaching the servers is 0\nThus, avg. can't be calculated(Divide by zero error)\n\n");
	}
	

	printf("\taverage number of packets in Q1 = %lf packets\n",(double)(sum_time_in_q1/tot_emulation_time));
	printf("\taverage number of packets in Q2 = %lf packets\n",(double)(sum_time_in_q2/tot_emulation_time));
	printf("\taverage number of packets at S1 = %lf packets\n",(double)(sum_time_in_s1/tot_emulation_time));
	printf("\taverage number of packets at S2 = %lf packets\n\n",(double)(sum_time_in_s2/tot_emulation_time));
	
	if(m_for_n!=0)
	{
		printf("\taverage time a packet spent in system = %lfs\n",(double)(sum_sys_time/(m_for_n*1000000.00)));
		//printf("pkt drop count %d\n",pkt_drop_count);
		v=sum_of_sq_time/m_for_n-((sum_sys_time/(m_for_n*1000000.00))*(sum_sys_time/(m_for_n*1000000.00)));
	}
	else
	{
		printf("\taverage time a packet spent in system = N/A because the no. of pkts reaching the servers is 0\nThus, avg. can't be calculated(Divide by zero error)\n");
		//printf("pkt drop count %d\n",pkt_drop_count);
		v=0;
	}
	v=sqrt(v);
	printf("\tstandard deviation for time spent in system = %lfs\n\n",v);
	printf("\ttoken drop probability = %lf\n",(double)(tok_drop_count*1.0)/(tot_tokens*1.0));
	printf("\tpacket drop probability = %lf\n\n",(double)(pkt_drop_count*1.0)/(num_packets*1.0));
}



void My402ListUnlinkForCtrlC(My402List *my_list, My402ListElem *list_elem, int qx) {
	double current_time=0.0;
	Obj *foo=(Obj*)(list_elem->obj);	
	list_elem->prev->next=list_elem->next;
	list_elem->next->prev=list_elem->prev;
	get_time(&current_time);
	print_time(current_time);
	printf("p%d removed from Q%d\n",foo->pkt_num,qx);
	free(list_elem->obj);	
	free(list_elem);
	my_list->num_members--;
 }





void My402ListUnlinkAllForCtrlC(My402List *my_list, int qx) {
	while(!My402ListEmpty(my_list))
		My402ListUnlinkForCtrlC(my_list, my_list->anchor.next,qx);
	return;
 }




void *interrupt_handler(void* arg)
{
	int sig;
	double current_time=0.0;
	while (TRUE) 
	{
		sigwait(&set, &sig);
		if(sig==SIGINT)
		{
			pthread_mutex_lock(&m);
			token_thread_cancel=1;
			thread_ended_ctrl_c=1;
			pthread_cancel(packet_t);
			pthread_cancel(token_t);
			
			printf("\n");
			get_time(&current_time);
			print_time(current_time);
			printf("SIGINT caught, no new packets or tokens will be allowed\n");
			My402ListUnlinkAllForCtrlC(q1,1);
			My402ListUnlinkAllForCtrlC(q2,2);
			pthread_cond_broadcast(&cv);
			pthread_mutex_unlock(&m);
			break;		
		}
	}
	return 0;
}



int main(int argc, char *argv[])
{
	int result;
	char buff[90];
	double curr_time=0.0;
	int numbers_read=0;
	double dummy1,dummy3;
	long dummy2;
	
	/*token_t=(pthread_t)malloc(sizeof(pthread_t));
	packet_t=(pthread_t)malloc(sizeof(pthread_t));
	s1_t=(pthread_t)malloc(sizeof(pthread_t));
	s2_t=(pthread_t)malloc(sizeof(pthread_t));*/
	
	sigemptyset(&set);
	sigaddset(&set,SIGINT);
	sigprocmask(SIG_BLOCK,&set, 0);
	
	q1=(My402List*)malloc(sizeof(My402List));
	My402ListInit(q1);

	q2=(My402List*)malloc(sizeof(My402List));
	My402ListInit(q2);

	q3=(My402List*)malloc(sizeof(My402List));
	My402ListInit(q3);
	
    	SetProgramName(*argv);
    	ProcessOptions(argc, argv);
	
	if(strcmp(tsfile,"")==0)
	{	
		printf("\nEmulation Parameters:\n");
		printf("\tnumber to arrive = %ld\n",n);
		printf("\tlambda = %.3lf\n",lambda);
		printf("\tmu = %.3lf\n",mu);
		printf("\tr = %.3lf\n",r);
		printf("\tB = %ld\n",B);
		printf("\tP = %ld\n\n",P);
	}
	else
	{
		stat(tsfile,&directory);
		if(S_ISDIR(directory.st_mode))
	  	{
		    printf("Input file \"%s\" is a directory\n",tsfile);
		    exit(1);
		}
		fp = fopen(tsfile, "r");
		if(fp==NULL)
		{
			if(errno==13)
				printf("Do not have permission to access file\n");
			else if(errno==2)
				printf("File specified not found\n");
			else
				printf("Specified file could not be processed\n");			
			exit(1);
		}
		fgets(buff,sizeof(buff),fp);
		sscanf(buff,"%ld\n",&n);
		m_for_n=n;
		while(fgets(buff,sizeof(buff),fp)!=NULL)
		{
			numbers_read=sscanf(buff,"%lf%ld%lf\n",&dummy1,&dummy2,&dummy3);
			if(numbers_read!=3)
			{
				printf("(input file is not in the right format)\n");
				fclose(fp);
				exit(1);
			}
			if(isnan(dummy1) || isnan(dummy2) || isnan(dummy3))
			{
				printf("(input file is not in the right format)\n");
				fclose(fp);
				exit(1);
			}
		}
		fclose(fp);
		printf("\nEmulation Parameters:\n");
		printf("\tr = %.3lf\n",r);
		printf("\tB = %ld\n",B);
		printf("\ttsfile = %s\n\n",tsfile);	
	
		
		fp = fopen(tsfile, "r");
		fgets(buff,sizeof(buff),fp);
		sscanf(buff,"%ld\n",&dummy2);
		///printf("n dummy %ld\n",dummy2);
	}
	if(r<0.1)
		r=0.1;
	get_time(&start_time);	
	print_time(start_time);
	printf("emulation begins\n");
	
	pthread_create(&cc_thread, 0,interrupt_handler, (void*)0);//ctrlc thread
	pthread_create(&token_t, 0, token_handler, (void*)0);
	
	pthread_create(&packet_t, 0, packet_handler, (void*)0);
	
	pthread_create(&s1_t, 0, server_handler1, (void*)1);
	pthread_create(&s2_t, 0, server_handler2, (void*)2);
	


	pthread_join(token_t, (void**)&result);
	//printf("token joined\n");
	pthread_join(packet_t, (void**)&result);
	//printf("packet joined\n");
	pthread_join(s1_t, (void**)&result);
	//printf("s1 joined\n");
	pthread_join(s2_t, (void**)&result);
	//printf("s2 joined\n");
	pthread_cancel(cc_thread);
	pthread_join(cc_thread, (void**)&result);
	//printf("cc joined\n");
	get_time(&curr_time);	
	print_time(curr_time);
	printf("emulation ends\n");

	print_statistics((double)(curr_time-start_time));
	
	
	//Traverse(q3);
	//free(token_t);//=(pthread_t)malloc(sizeof(pthread_t));
	//free(packet_t);//=(pthread_t)malloc(sizeof(pthread_t));
	//free(s1_t);//=(pthread_t)malloc(sizeof(pthread_t));
	if(strcmp(tsfile,"")!=0)
		fclose(fp);
	My402ListUnlinkAll1(q3);
	///printf("mainend\n");
    	return(0);
}
