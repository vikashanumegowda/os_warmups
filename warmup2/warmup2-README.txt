Documentation for Warmup Assignment 2
=====================================

+-------+
| BUILD |
+-------+

Comments: make (or) make warmup2 must be used to compile and link the files.
./warmup2 and other command line arguments to run the program.


+---------+
| GRADING |
+---------+

Basic running of the code : 100 out of 100 pts

Missing required section(s) in README file : None
Cannot compile : None
Compiler warnings : None
"make clean" : works
Segmentation faults : None
Separate compilation : Done
Using busy-wait : No
Handling of commandline arguments:
    1) -n : Yes
    2) -lambda : Yes
    3) -mu : Yes
    4) -r : Yes
    5) -B : Yes
    6) -P : Yes
Trace output :
    1) regular packets: Yes
    2) dropped packets: Yes
    3) removed packets: Yes
    4) token arrival (dropped or not dropped): Displayed with timestamp of the event
Statistics output :
    1) inter-arrival time : Calculated and displayed
    2) service time : Calculated and displayed
    3) number of customers in Q1 :double used instead of long int (some error stats are shown)
    4) number of customers in Q2 : Calculated and displayed
    5) number of customers at a server : Calculated and displayed
    6) time in system : Calculated and displayed
    7) standard deviation for time in system : Calculated and displayed
    8) drop probability : Calculated and displayed
Output bad format : Very very rare (only on nunki).
Output wrong precision for statistics (should be 6-8 significant digits) : Required precision maintained
Large service time test : Correctly executes
Large inter-arrival time test : Correctly executes
Tiny inter-arrival time test : Correctly executes
Tiny service time test : Correctly executes
Large total number of customers test : Correctly executes
Large total number of customers with high arrival rate test : Correctly executes
Dropped tokens test : Correctly executes
Cannot handle <Cntrl+C> at all (ignored or no statistics) : Cntrl+C implemented
Can handle <Cntrl+C> but statistics way off : Cntrl+C implemented with statistics very slightly off
Not using condition variables and do some kind of busy-wait : NO
Synchronization check : Synchronization correctly implemented
Deadlocks : Never

+----------------------+
| BUGS / TESTS TO SKIP |
+----------------------+

Is there are any tests in the standard test suite that you know that it's not
working and you don't want the grader to run it at all so you won't get extra
deductions, please list them here.  (Of course, if the grader won't run these
tests, you will not get plus points for them.)

No tests to be skipped.

+------------------+
| OTHER (Optional) |
+------------------+

Comments on design decisions: 

I have used a separate list Q3 to collect all the packets' details to compute the statistics of the system. The packets from Q2 will be served in one of the servers and then queued into this Q3 in chronological order (when that packet leaves the servers-stage).


