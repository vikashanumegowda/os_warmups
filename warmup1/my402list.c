/******************************************************************************/
/* Important CSCI 402 usage information:                                      */
/*                                                                            */
/* This fils is part of CSCI 402 programming assignments at USC.              */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

/*
 * Author:      William Chia-Wei Cheng (bill.cheng@acm.org)
 *
 * @(#)$Id: listtest.c,v 1.1 2016/12/21 19:22:34 william Exp $
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include "cs402.h"

#include "my402list.h"

int  My402ListLength(My402List *my_list){
	return my_list->num_members;
 }

int  My402ListEmpty(My402List *my_list) {
	if (my_list->num_members>0)
		return FALSE;
	return TRUE;
 }


int  My402ListAppend(My402List *my_list, void *obj) {
	My402ListElem* elem = (My402ListElem*) malloc(sizeof (My402ListElem));
	if (elem==NULL)
		return FALSE;
	elem->obj=obj;
	if(My402ListEmpty(my_list))
	{
		my_list->anchor.next=elem;
		my_list->anchor.prev=elem;
		elem->next=&my_list->anchor;
		elem->prev=&my_list->anchor;
		my_list->num_members=1;	
	}
	else
	{
		my_list->num_members++;
		my_list->anchor.prev->next=elem;
		elem->next=&my_list->anchor;
		elem->prev=my_list->anchor.prev;
		my_list->anchor.prev=elem;
	}
	return TRUE;
 }

int  My402ListPrepend(My402List *my_list, void *obj) {
	My402ListElem* elem =(My402ListElem*) malloc(sizeof (My402ListElem));
	if (elem==NULL)
		return FALSE;	
	elem->obj=obj;
	if(My402ListEmpty(my_list))
	{
		my_list->anchor.next=elem;
		my_list->anchor.prev=elem;
		elem->next=&my_list->anchor;
		elem->prev=&my_list->anchor;
		my_list->num_members=1;	
	}
	else
	{
		my_list->num_members++;
		my_list->anchor.next->prev=elem;
		elem->prev=&my_list->anchor;
		elem->next=my_list->anchor.next;
		my_list->anchor.next=elem;
	}
	return TRUE;
 }

void My402ListUnlink(My402List *my_list, My402ListElem *list_elem) {
	list_elem->prev->next=list_elem->next;
	list_elem->next->prev=list_elem->prev;
	free(list_elem);
	my_list->num_members--;
 }

void My402ListUnlinkAll(My402List *my_list) {
	while(!My402ListEmpty(my_list))
		My402ListUnlink(my_list, my_list->anchor.next);
	return;
 }

int  My402ListInsertAfter(My402List *my_list, void *obj, My402ListElem *list_elem) {
	My402ListElem* elem = (My402ListElem*)malloc(sizeof (My402ListElem));
	if (elem==NULL)
		return FALSE;	
	elem->obj=obj;
	elem->next=list_elem->next;
	elem->prev=list_elem;
	list_elem->next->prev=elem;
	list_elem->next=elem;
	my_list->num_members++;
	return TRUE;
 }

int  My402ListInsertBefore(My402List *my_list, void *obj, My402ListElem *list_elem) {
	My402ListElem* elem = (My402ListElem*)malloc(sizeof (My402ListElem));
	if (elem==NULL)
		return FALSE;	
	elem->obj=obj;
	elem->next=list_elem;
	elem->prev=list_elem->prev;
	list_elem->prev->next=elem;
	list_elem->prev=elem;
	my_list->num_members++;
	return TRUE;
 }


My402ListElem* My402ListFirst(My402List *my_list) {
	if(my_list->num_members>0)
		return my_list->anchor.next;	
	return NULL;
 }

My402ListElem* My402ListLast(My402List *my_list) {
	if(my_list->num_members>0)
		return my_list->anchor.prev;	
	return NULL;
 }

My402ListElem* My402ListNext(My402List *my_list, My402ListElem *list_elem) {
	if (list_elem->next==&my_list->anchor)
		return NULL;	
	return list_elem->next;
 }

My402ListElem* My402ListPrev(My402List *my_list, My402ListElem *list_elem) {
	if (list_elem->prev==&my_list->anchor)
		return NULL;	
	return list_elem->prev;
 }


My402ListElem* My402ListFind(My402List *my_list, void *obj) {
	My402ListElem* head=my_list->anchor.next;
	while (head!=NULL)
	{
		if (head->obj==obj)
			return head;
		head=head->next;
	}
	return NULL;
 }


int My402ListInit(My402List *my_list) {
	my_list->num_members=0;
	my_list->anchor.next=NULL;
	my_list->anchor.prev=NULL;
	my_list->anchor.obj=NULL;
	return TRUE;
 }

