Documentation for Warmup Assignment 1
=====================================

+-------+
| BUILD |
+-------+

Comments: Unedited Makefile, command to be run to compile : make 

+-----------------+
| SKIP (Optional) |
+-----------------+

Nothing to skip.

+---------+
| GRADING |
+---------+

(A) Doubly-linked Circular List : 40 out of 40 pts

(B.1) Sort (file) : 30 out of 30 pts
(B.2) Sort (stdin) : 30 out of 30 pts

Missing required section(s) in README file : None
Cannot compile : False (Compilation takes place)
Compiler warnings : None
"make clean" : Works
Segmentation faults : None
Separate compilation : None
Malformed input : All conditions considered.
Too slow : No
Bad commandline : All conditions are considered.
Did not use My402List and My402ListElem to implement "sort" in (B) : False.

+------+
| BUGS |
+------+

Comments: None

+------------------+
| OTHER (Optional) |
+------------------+

Comments on design decisions: No changes from given spec except the use of an extra header file which is included in appropriate files.
Comments on deviation from spec: One Extra header file for object element structure, written for ease of use.

