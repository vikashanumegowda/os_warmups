/******************************************************************************/
/* Important CSCI 402 usage information:                                      */
/*                                                                            */
/* This fils is part of CSCI 402 programming assignments at USC.              */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

/*
 * Author:      William Chia-Wei Cheng (bill.cheng@acm.org)
 *
 * @(#)$Id: listtest.c,v 1.1 2016/12/21 19:22:34 william Exp $
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>
#include <math.h>
#include <locale.h>
#include <errno.h>

#include "cs402.h"

#include "my402list.h"
#include "my402transaction.h"

char* truncat(char* str, int n)
{
	if(n>strlen(str)+1)
		n=strlen(str);
	char *new =(char*) malloc (sizeof (char) * n);
	
	int a;
	
	for(a=0;a<n;a++)
		new[a]=str[a];
	new[a]='\0';
	return new;
}

void Traverse(My402List *list)
{
	/*time_t curtime=(time_t)1230738833;//Wed Dec 31 07:53:53 2008
	//setlocale(LC_ALL,"en_US");
	char *time_str=ctime(&curtime);
	char w[3],m[3],t[8];
	char time_str_1[15];
	
	sscanf(time_str,"%s %s %d %s %d",w,m,&d,t,&y);
	sprintf(time_str_1,"%s %s % 2d %d",w,m,d,y);
   	printf("Current time = %s\n", time_str_1);   	
	*/
	My402ListElem *elem=NULL;
	char *time_str=(char*)calloc(16,sizeof(char));
	char *w=(char*)calloc(4,sizeof(char));
	char *m=(char*)calloc(4,sizeof(char));
	char *t=(char*)calloc(8,sizeof(char));	
	int d=0,y=0;
	char *time_str_1=(char*)calloc(16,sizeof(char));
	time_t forCTime;
	double balance=0;
	printf("+-----------------+--------------------------+----------------+----------------+\n");
	printf("|       Date      | Description              |         Amount |        Balance |\n");
	printf("+-----------------+--------------------------+----------------+----------------+\n");
	for (elem = My402ListFirst(list); elem != NULL; elem = My402ListNext(list, elem))
	{
		
		My402TransactionElem *foo = (My402TransactionElem*)(elem->obj);
		forCTime=(time_t)foo->time;
		/*//from discussion pdf (don't know how to use this)
		char date[16];
		char buf[26];
		strncpy(buf, ctime(...), sizeof(buf));	*/	
		time_str=(char *)ctime(&forCTime);
		sscanf(time_str,"%s %s %d %s %d",w,m,&d,t,&y);
		sprintf(time_str_1,"%s %s %2d %d",w,m,d,y);
		//char *trun_str=truncat(foo->description,25);
		printf("| %s | %-24s | ",time_str_1,truncat(foo->description,25));
		//sfree(trun_str);		
		if (foo->type=='+')
		{
			balance+=foo->amount;
			if (foo->amount>=10000000)
				printf(" ?,?\??,?\??.\??  | ");
			else if(foo->amount<10000000 && foo->amount>=0)
				printf("%'13.2lf  | ",foo->amount);
			else 
			{
				perror("Invalid amount (cannot be negative)\n");
				exit(1);			
			}	
		
		}
		else if(foo->type=='-')
		{
			balance-=foo->amount;
			if (foo->amount>=10000000)
				printf("(?,?\?\?,?\?\?.\?\?) | ");
			else if(foo->amount<10000000 && foo->amount>=0)
				printf("(%'12.2lf) | ",foo->amount);
			else 
			{
				perror("Invalid amount (cannot be negative)\n");
				exit(1);			
			}
		}
		
		if (balance>=0 && balance<10000000)
			printf("%'13.2lf  |\n",balance);
		else if(balance>=10000000)
			printf(" ?,?\??,?\??.\??  |\n");
		else if(balance<=-10000000)
			printf("(?,?\?\?,?\?\?.\?\?) |\n");
		else		
			printf("(%'12.2lf) |\n",fabs(balance));

	}
	free(w);
	free(m);
	free(t);
	printf("+-----------------+--------------------------+----------------+----------------+\n");
}

static
void BubbleForward(My402List *pList, My402ListElem **pp_elem1, My402ListElem **pp_elem2)
    /* (*pp_elem1) must be closer to First() than (*pp_elem2) */
{
    My402ListElem *elem1=(*pp_elem1), *elem2=(*pp_elem2);
    void *obj1=elem1->obj, *obj2=elem2->obj;
    My402ListElem *elem1prev=My402ListPrev(pList, elem1);
/*  My402ListElem *elem1next=My402ListNext(pList, elem1); */
/*  My402ListElem *elem2prev=My402ListPrev(pList, elem2); */
    My402ListElem *elem2next=My402ListNext(pList, elem2);

    My402ListUnlink(pList, elem1);
    My402ListUnlink(pList, elem2);
    if (elem1prev == NULL) {
        (void)My402ListPrepend(pList, obj2);
        *pp_elem1 = My402ListFirst(pList);
    }
    else
    {
      (void)My402ListInsertAfter(pList, obj2, elem1prev);
        *pp_elem1 = My402ListNext(pList, elem1prev);
    }
    if (elem2next == NULL) {
        (void)My402ListAppend(pList, obj1);
        *pp_elem2 = My402ListLast(pList);
    }
    else
      {
        (void)My402ListInsertBefore(pList, obj1, elem2next);
        *pp_elem2 = My402ListPrev(pList, elem2next);
    }
}


static
void BubbleSortForwardList(My402List *pList, int num_items)
{
    My402ListElem *elem=NULL;
    int i=0;

    if (My402ListLength(pList) != num_items) {
        fprintf(stderr, "List length is not %1d in BubbleSortForwardList().\n", num_items);
        exit(1);
    }
    for (i=0; i < num_items; i++) {
        int j=0, something_swapped=FALSE;
        My402ListElem *next_elem=NULL;

        for (elem=My402ListFirst(pList), j=0; j < num_items-i-1; elem=next_elem, j++) {
		My402TransactionElem *t_elem=(My402TransactionElem*) elem->obj;          
		unsigned int cur_val=t_elem->time, next_val=0;

            	next_elem=My402ListNext(pList, elem);
		My402TransactionElem *t_next_elem=(My402TransactionElem*)next_elem->obj;
            	next_val = (unsigned int)(t_next_elem->time);

            if (cur_val > next_val) {
                BubbleForward(pList, &elem, &next_elem);
                something_swapped = TRUE;
            }
		else if(cur_val == next_val)
		{
			printf("Same timestamps on line %d and line %d\nSystem doesn't support more than one transactions at the same time\n",t_next_elem->sl_no,t_elem->sl_no);
			//printf("Hello World\n");
			exit(1);
		}
        }
        if (!something_swapped) break;
    }
}


int main(int argc, char *argv[])
{
	/*
	done file or stdin
	done My420List list;
    	done if (!My402ListInit(&list)) {  error  }
    	if (!ReadInput(fp, &list)) {  error  }
    	done if (fp != stdin) fclose(fp);
    	done SortInput(&list);
    	done PrintStatement(&list);
	*/



        setlocale(LC_NUMERIC,"en_US");
        char buff[1026];
	char type;
	unsigned int ttime;
	double amount;
	char desc[25];
	//char error_msg[29];
	int i,n=0,line_number=0;
	FILE *fp;
	time_t mytime;
	char *str_vars[6];
	char *token,*left,*right;

	if (argc==2)
	{
		if(strcmp(argv[1],"sort")!=0)
		{
			printf("Usage: ./warmup1 sort [file]\n");
			exit(1);
		}
		fp = stdin;
		if(fp==NULL)
		{
			printf("STDIN can't be accessed\n");
			printf("Usage: ./warmup1 sort [file]\n");
			exit(1);
		}
	}
	else if (argc==3)
	{
		if(strcmp(argv[1],"sort")!=0)
		{
			printf("Usage: ./warmup1 sort [file]\n");
			exit(1);
		}
	  
		//{
		//}
		//if(argv[2]==NULL)
		//	fp=stdin;
		fp = fopen(argv[2], "r");
		if(fp==NULL)
		{
			if(errno==13)
				printf("Do not have permission to access file\n");
			if(errno==2)
				printf("File specified not found\n");
			exit(1);
		}
	}
	else
	  {printf("Malformed command\n");
	  exit(1);}
	My402List *my_list=(My402List*)malloc(sizeof(My402List));
	//FILE *f=fopen("test.tfile","r");
	if(!My402ListInit(my_list))
	{
		perror("List init malloc failed");
		exit(1);
	}
	while(fgets(buff,sizeof(buff),fp)!=NULL)
	{	
		//buff length check 1024
		line_number=line_number+1;
		if(strlen(buff)>1024)
		{
			printf("Error on line %d in the input file\n",line_number);
		    	perror("The transction has more than 1024 chars\n");
		    	exit(1);
		}
				
		token=strtok(buff,"\t");
			
		n=0;
		while(token!=NULL)
		{	
			str_vars[n]=token;
			token=strtok(NULL,"\t");
			n++;
			
		}

		if(n>4)
		{
			printf("Malformed input(number of fields) on line %d in the input file\n",line_number);
			//perror(error_msg);
			exit(1);
		}
		else if(n<4)
		{
			printf("Malformed input(number of fields) on line %d in the input file\n",line_number);
			//perror(error_msg);
			exit(1);
		}
		type=str_vars[0][0];
		if(!(type=='-' || type=='+'))
		  {
		    printf("Error on line %d in the input file\n",line_number);
		    perror("Unexpected type of transaction");
		    exit(1);
		  }
		ttime=strtoul(str_vars[1],NULL,10);
		mytime = time(NULL);
		if(ttime>(unsigned)mytime)
		  {
		    printf("Error on line %d in the input file\n",line_number);
		    perror("Time of transaction cannot be in the future!");
		    exit(1);
		  }
		strcpy(desc,str_vars[3]);
		i=0;
		for(i=0;i<strlen(desc);i++)
			if(desc[i]=='\n')
				desc[i]='\0';
		desc[24]='\0';
		amount=atof(str_vars[2]);
		left=strtok(str_vars[2],".");
		if(strlen(left)>7)
		{
			printf("Too great an amount entered in line %d, it should be less than or equal to 9,999,999\n",line_number);
			exit(1);
		}
		right=strtok(NULL,"\0");
		if(strlen(right)>2)
		{
			printf("Amount in line %d can't have more than 2 digits after \'.\'\n",line_number);
			exit(1);
		}
		//amount=atof(str_vars[2]);
		My402TransactionElem* t_elem = (My402TransactionElem*)malloc(sizeof (My402TransactionElem));
		t_elem->type=type;
		t_elem->time=ttime;
		t_elem->sl_no=line_number;
		t_elem->amount=amount;
		strcpy(t_elem->description,desc);
		n=My402ListAppend(my_list, t_elem);
		//field_num=0;
		n=0;
	}
	if(fp != stdin)
		fclose(fp);
	BubbleSortForwardList(my_list, my_list->num_members);
	Traverse(my_list);
	free(my_list);
	return 0;
}
