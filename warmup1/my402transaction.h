#include<sys/time.h>

#ifndef _MY402TRANSACTION_H_
#define _MY402TRANSACTION_H_

#include "cs402.h"

typedef struct tagMy402TransactionElem {
	int sl_no;	
	char type;
	unsigned int time;
	double amount;
	char description[25];	
    
} My402TransactionElem;

#endif
/*_MY402LIST_H_*/
